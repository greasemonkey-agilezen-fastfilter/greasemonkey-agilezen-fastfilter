// ==UserScript==
// @name           AgileZen-quickfilter
// @namespace      https://github.com/hoxu/
// @description    AgileZen quick filtering, immediately shows the filter list in board view
// @include        https://agilezen.com/project/*/board
// ==/UserScript==

waitForJquery();
function waitForJquery() {
  if (typeof unsafeWindow.jQuery == 'undefined')
    window.setTimeout(waitForJquery, 100);
  else
    unsafeWindow.jQuery(function() { init(unsafeWindow.jQuery); });
}

function init($) {
  $('.filter-toggle').click();
  $('#filter-selector').click();
}
